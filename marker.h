#ifndef MARKER_H
#define MARKER_H

class QPoint;

class Marker
{
public:
    Marker(int ax, int ay, int bx, int by, bool red);

private:
    int x;
    int y;
    bool red;
};

#endif // MARKER_H
