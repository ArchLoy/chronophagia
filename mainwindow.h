#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

class Clock;

namespace Ui {
class MainWindow;
}

class QBoxLayout;
class QPushButton;
class QTime;
class QLabel;


class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    void timerEvent(QTimerEvent *event) override;
    ~MainWindow();

private:
    Ui::MainWindow *ui;

    Clock* m_clock;

    QBoxLayout* m_layout;
    QBoxLayout* m_btnLayout;

    QPushButton* m_startBtn;
    QPushButton* m_stopBtn;
    QPushButton* m_resetBtn;
    QLabel* m_timeLabel;

    int m_timerId;
    bool started;
    int m_ellapsedms;

    QTime* m_timer;

public slots:
    void start();
    void stop();
    void reset();
};

#endif // MAINWINDOW_H
