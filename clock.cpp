#include "clock.h"

#include <QPainter>
#include <QRectF>
#include <QPoint>
#include <cmath>

#include <QDebug>

static std::pair<QPoint, QPoint> deco[60];

const double rad = std::acos(-1)/180;

Clock::Clock(QWidget *parent) : QWidget(parent)
{
    this->setFixedSize(300, 300);

    int r1 = 136;  // External radius
    int r2 = 130;  // Internal radius

    double c = 300.0/2.0;
    float angle = 0.0f;
    float delta = 1.0f*(6.0f*(rad));

    for(int i =0; i<60; i++)
    {
        if(i%5 == 0)
            r2 -=5;

        QPoint p1;
        p1.setX(c+ r1*std::cos(angle));
        p1.setY(c+ r1*std::sin(angle));

        qDebug() << p1;
        QPoint p2;
        p2.setX(c+ (float)r2*std::cos(angle));
        p2.setY(c+ (float)r2*std::sin(angle));
        deco[i] = std::pair<QPoint, QPoint>(p1, p2);

        if(!(i%5))
            r2 +=5;

        angle+=delta;
    }
    m_h = new QPoint(c+float(r1)*0.7f*std::cos(90.0f*(rad)), c+float(r1)*0.7f*std::sin(-90.0*rad));
    m_m = &deco[45].second;
    m_s = &deco[45].first;
}

void Clock::calculate(int sec, int min, int hour)
{
    if(sec != m_curTime.s)
    {
        m_curTime.s = sec;
        if(sec<15)
            sec = 45+sec;
        else
            sec -= 15;

        m_s = &deco[sec].first;


        if(min!=m_curTime.m)
        {
            m_curTime.m = min;
            if(min<15)
                min += 45;
            else
                min -=15;
            m_m = &deco[min].second;

            // float permin = (float)hour+(float)m_curTime.m/60.0f;
            // permin = permin*(360.0/60.0);

            float permin = (float)(6*hour) + (float)m_curTime.m/10.0;
            int c = width()/2;
            m_h->setX(c+float(136)*0.8f*std::cos((permin-90.0f)*rad));
            m_h->setY(c+float(136)*0.8f*std::sin((permin-90.0f)*rad));
            m_curTime.h = hour;
        }
        this->update();
    }
}

void Clock::paintEvent(QPaintEvent *evt)
{
    Q_UNUSED(evt)

    QPainter p(this);
    QPen pen;
    pen.setWidth(5);
    p.setPen(pen);
    p.setRenderHint(QPainter::Antialiasing, true);
    p.drawEllipse(QPoint(width()/2, height()/2), 140, 140);
    pen.setWidth(1);
    p.setPen(pen);
//    p.drawEllipse(250, 250, 200, 200);
    for(int i=0; i<60; i++)
    {
        if(!(i%5))
        {
            pen.setColor(QColor(255, 0, 0));
            pen.setWidth(2);
            p.setPen(pen);
        }
        p.drawLine(deco[i].second, deco[i].first);

        if(!(i%5))
        {
            pen.setColor(QColor(0,0,0));
            pen.setWidth(1);
            p.setPen(pen);
        }
    }

    pen.setColor(QColor(0,255,0));
    p.setPen(pen);
    p.drawLine(QPoint(width()/2, height()/2), *m_s);

    pen.setColor(QColor(0,0,255));
    p.setPen(pen);
    p.drawLine(QPoint(width()/2, height()/2), *m_m);

    pen.setColor(QColor(255,0,0));
    p.setPen(pen);
    p.drawLine(QPoint(width()/2, height()/2), *m_h);
}
