#ifndef CLOCK_H
#define CLOCK_H

#include <QWidget>

#include <vector>


class QPainter;
class QPoint;


struct Time{
    int h = 0;
    int m = 0;
    int s = 0;
};


class Clock : public QWidget
{
    Q_OBJECT
public:
    explicit Clock(QWidget *parent = 0);

    QPainter* mpaint;

    void calculate(int sec, int min, int hour);


private:
    QPoint* m_h;
    QPoint* m_m;
    QPoint* m_s;

    Time m_curTime;

signals:

public slots:

protected:
    void paintEvent(QPaintEvent* evt) override;
};

#endif // CLOCK_H
