#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QBoxLayout>
#include <QPushButton>
#include <QTime>
#include <QLabel>


#include "clock.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    m_layout = new QBoxLayout(QBoxLayout::TopToBottom, centralWidget());
    m_btnLayout = new QBoxLayout(QBoxLayout::LeftToRight);
    m_clock = new Clock(this);

    m_startBtn = new QPushButton("Start", this);
    m_stopBtn = new QPushButton("Stop", this);
    m_resetBtn = new QPushButton("Reset", this);

    m_stopBtn->setDisabled(true);
    m_resetBtn->setDisabled(true);

    connect(m_startBtn, &QPushButton::clicked, this, &MainWindow::start);
    connect(m_stopBtn, &QPushButton::clicked, this, &MainWindow::stop);
    connect(m_resetBtn, &QPushButton::clicked, this, &MainWindow::reset);

    m_btnLayout->addWidget(m_startBtn);
    m_btnLayout->addWidget(m_stopBtn);
    m_btnLayout->addWidget(m_resetBtn);

    m_timeLabel = new QLabel("00:00:00", this);

    m_layout->addWidget(m_timeLabel);
    m_layout->addLayout(m_btnLayout);
    m_layout->addWidget(m_clock);

    m_timer = new QTime();
    started = false;
    m_ellapsedms = 0;
    m_timerId = -1;
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::start()
{
    if(m_timerId == -1)
    {
        m_timerId = startTimer(100);
        m_timer->start();
    }
    m_startBtn->setDisabled(true);
    m_stopBtn->setDisabled(false);
}

void MainWindow::stop()
{
    killTimer(m_timerId);
    m_timerId = -1;
    m_startBtn->setDisabled(false);
    m_stopBtn->setDisabled(true);
    m_resetBtn->setDisabled(false);
}

void MainWindow::reset()
{
    m_timeLabel->setText("00:00:00");
    m_ellapsedms = 0;
    m_resetBtn->setDisabled(true);
    m_clock->calculate(0, 0, 0);
}

void MainWindow::timerEvent(QTimerEvent *event)
{
    Q_UNUSED(event);

    m_ellapsedms += m_timer->elapsed()+15000; // TODO: Remove +15000 for real time test :D
    m_timer->restart();

    int h = (m_ellapsedms/3600000)%24;
    int m = (m_ellapsedms/60000)%60;
    int s = (m_ellapsedms/1000)%60;
    int ms = (m_ellapsedms%1000)/10;
    QLatin1Char z('0');
    m_timeLabel->setText(QString("%1:%2:%3:%4").arg(h, 2, 10, z).arg(m, 2, 10, z).arg(s, 2, 10, z).arg(ms, 2, 10, z));
    m_clock->calculate(s, m, h);
}
